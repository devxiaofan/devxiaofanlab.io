---
title: 手写 call、apply及bind函数
top: false
cover: false
toc: true
mathjax: true
date: 2020-05-18 23:07:43
password:
summary:
tags: JavaScript
categories: JavaScript
---

> call、apply及bind函数内部实现是怎样的？

- 不传入第一个参数，那么上下文默认为`window`
- 改变了`this`指向，让新的对象可以执行函数，并能接受参数

<!-- more -->

实现call**


```
Function.prototype.myCall = function (context) {
    // 如果this的类型不是function
    if (typeof this !== 'function') {
        throw new TypeError('error')
    }

    context = context || window
    context.fn = this
    const args = [...arguments].slice(1)
    const result = context.fn(...args)
    delete context.fn
    return result
}
```

分析：

- 首先`context`为可选参数，如果不传的话默认上下文为window 
- 接下来给`context`创建一个`fn`属性，并将值设置为需要调用的函数
- 因为`call`可以传入多个参数作为调用函数的参数，所以需要将参数剥离出来
- 然后调用函数并将对象上的函数删除

**实现apply**


```
Function.prototype.MyApply = function (context) {
    if (typeof this !== 'function') {
        throw new TypeError('error')
    }

    context = context || window
    context.fn = this
    let result
    if (arguments[1]) {
        result = context.fn(...arguments[1])
    } else {
        result = context.fn()
    }
    delete context.fn
    return result
}
```

实现过程和call类似

**实现bind**


```
Function.prototype.myMyBind = function (context) {
    if (typeof this !== 'function') {
        throw new TypeError('error')
    }

    const _this = this
    const args = [...arguments].slice(1)

    // 返回一个函数
    return function F() {
        if (this instanceof F) {
            return new _this(...args, ...arguments)
        }
        return _this.apply(context, args.concat(...arguments))
    }
}
```

实现分析： 

- 前几步和之前的实现差不多
- `bind`返回了一个函数，对于函数来说有两种方式调用，一种是直接调用，一种是通过`new`的方式
- 对于直接调用来说，这里选择了`apply`的方式实现，但是对于参数需要注意以下情况：因为`bind`可以实现类似这样的代码`f.bind(obj, 1)(2)`, 所以我们需要将两边的参数拼接起来，于是就有了这样的实现`args.concat(...arguments))`
- 通过`new`的方式，在之前的章节中我们学习过如何判断`this`，对于`new`的情况来说，不会被任何方式改变`this`，所以对于这种情况我们需要忽略传入的`this`

## new

> new的原理是什么？ 通过new的方式创建对象和通过字面量创建有什么区别？

在调用`new`的过程中会发生以下四件事情：

1. 新生成了一个对象
2. 链接到原型
3. 绑定this
4. 返回新对象

根据以上几个过程，可以实现一个`new`


```
function create() {
    let obj = {}
    let Con = [].shift.call(arguments)
    obj.__proto__ = Con.prototype
    let result = Con.apply(obj, arguments)
    return result instanceof Object ? result : obj
}
```

实现分析：

- 创建一个空对象
- 获取构造函数
- 设置空对象的原型
- 绑定`this`并执行构造函数
- 确保返回值为对象

对于对象来说，其实都是通过`new`产生的，无论是`function Foo()` 还是`let a = {}`

对于创建一个对象来说，更推荐使用字面量的方式创建对象
因为你使用`new Object()`的方式创建对象需要通过作用域链一层层找到`Object`,但是你使用字面量的方式就没有这个问题

## instanceof 的原理

`instanceof` 可以正确的判断对象的类型,因为内部的机制是判断对象的原型链中是不是能找到类型的`prototype`

手动实现`prototype`

```
function myInstanceof(left, right) {
    let prototype = right.prototype
    left = left.__proto__
    while (true) {
        if (left === null || left === undefined)
            return false
        if (prototype === left)
            return true
        left = left.__proto__
    }
}
```

实现的分析： 

- 首先实现类型的原型
- 然后获得对象的原型
- 然后一直循环判断对象的原型是否等于类型的原型， 直到对象的原型为`null`，因为原型链最终为`null`

## 为什么 0.1 + 0.2 != 0.3

因为是二进制精度裁剪的问题造成的

**解决**


```
parseFloat((0.1 + 0.2).toFixed(10)) === 0.3 // true
```

## 垃圾回收机制

> V8 下的垃圾回收机制是怎么样的？

V8 实现了准确式GC， GC 算法采用了分代式垃圾回收机制。因此，V8将内存（堆）分为新生代和老生带两部分