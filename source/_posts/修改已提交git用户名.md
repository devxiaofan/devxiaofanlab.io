---
title: 修改已提交git用户名
top: false
cover: false
toc: true
mathjax: true
date: 2020-06-05 17:04:53
password:
summary:
tags: tools
categories: tools
---

> 事出有因， 公司项目中git提交信息需要和自己的公司账号对应， 所以当时无脑用`SourceTree` 全局修改了， 后来发现自己的项目提交信息也变了， 发现的时候已经提交过一些记录了，所以才有了这个精力



<!-- more -->

## 具体步骤



### 列出要修改的提交信息

首先要确定需要修改的提交信息在历史几条记录之内

![image-20200605171809075](http://imgs.devzhangjs.com/091809.png)



例如我这里在3条之内， 记住要修改记录的ID， 然后就可以在终端运行此命令：





```
git rebase -i HEAD~3
```



### 修改提交信息前缀



我配置了`git`默认编辑软件为 `sublime`， 可直接修改， 不配置的话需要在终端通过`vim`命令修改内容；

就是把需要修改的记录前面的 `pick` 修改为 `edit`

![image-20200605171943250](http://imgs.devzhangjs.com/091943.png)

修改之后

![image-20200605172017053](http://imgs.devzhangjs.com/092017.png)





保存退出



### 提交修改的commit信息

这时你会在终端看到这个界面

![image-20200605172422218](http://imgs.devzhangjs.com/092422.png)



意思是可以用`git commit --amend`来修改这次提交， 改完之后， 可以用 `git rebase --continue` 继续。

但是我需要修改的是提交的作者信息， 所以这样是不行的， 需要使用 `git commit --amend --author='名字 <邮箱>' --no-edit` 才可以。 其中名字是要修改为什么， 邮箱外面要加`<>`。  运行后是这样





![image-20200605172831432](http://imgs.devzhangjs.com/092831.png)



然后就可以运行继续命令了



### 继续操作



```
git rebase --continue
```



因为这个是一条一条改的， 所以会出现这个

![image-20200605172947388](http://imgs.devzhangjs.com/092948.png)



重复一次上面的操作就行了



### 提交同步



最后把提交记录同步到服务器



```
git push --force
```



### 验证修改



再次查看提价记录， 已经都改过来了



![image-20200605173127794](http://imgs.devzhangjs.com/093128.png)

至此， 大功告成



### 命令总结



```
# 第一步，（n）代表要展示的提交次数
git rebase -i HEAD~n
# 第二步
编辑提交信息，把`pick` 改成 `edit`，保存退出
# 第三步
git commit --amend --author="作者 <邮箱>" --no-edit
# 第四步
git rebase --continue
# 第五步
git push --force
```

