---
title: '状态机笔记'
top: false
cover: false
toc: true
mathjax: true
date: 2020-05-09 15:25:02
updated: 2020-05-10 17:40:59
password:
summary:
tags: 'JavaScript'
categories: 'JavaScript'
---
## 定义

> 状态机是有限状态自动机的简称， 是现实事物运行规则抽象而成的一个数学模型

### 状态（state）

<!-- more -->

现实事物是有不同状态的。 我们通常所说的状态机是有限状态机， 也就是被描述的事物的状态的数量是有限的

状态机， 也就是 state machine， 不是指一台实际机器，而是指一个数学模型。说白了， 一般就是指一张状态转化图。

状态机全程 有限状态自动机。 给定一个状态机， 同时给定它的初识状态和输入， 那么输出状态是可以运算出来的。 



## 四大概念

状态机的四大概念

- State，状态。 一个状态机至少包含两个状态
- Event， 事件。 事件就是执行某个操作的触发条件或者口令。 
- Action， 动作。 事件发生以后要执行的动作。编程的时候，一个action一般对应一个函数。
- Transition， 变换。 也就是从一个状态变成另一个状态。 



## 应用

状态机是一个对真是世界的抽象， 而且是逻辑严谨的数学抽象， 所以明显非常适合用于数学领域。 也可以用在硬件设计、编译器设计，以及编程实现各种具体业务逻辑的时候



## 总结

状态机不是实际的机器设备，而是一个数学模型，通常体现为一个状态转换图。 涉及相关概念是State 状态， Event 事件, Action 动作， Transition 转换。 状态机是计算机科学的重要基础概念之一， 也可以说是一种总结归纳问题的思想