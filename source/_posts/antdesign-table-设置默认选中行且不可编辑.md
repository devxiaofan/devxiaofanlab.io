---
title: AntDesign Table 设置默认选中行且不可编辑
top: false
cover: false
toc: true
mathjax: true
date: 2020-06-19 17:26:17
password:
summary:
tags: tools
categories: tools
---



> 近日碰到一个需求， 就是在一个弹出的列表里需要默认勾选已经绑定过的数据并且不可取消，所以才有了这次记录



## 自定义rowSelection属性



查阅官方文档， 发现有这个途径， 那就开始自定义吧



<!-- more -->



### 代码



```JavaScript
const rowSelection = {
	type: 'checkbox', 
	selectionRowKeys: [...selectedRows, ...oldBindList],
	onChange: this.handleSelectRows, 
	// 根据一定规则设置每一行的编辑状态, 比如默认选中的行
	getCheckboxProps(record) {
		return {
			disabled: oldBindList.indexOf(record.id) !== -1,
		};
	},
};
```



### 代码解读

`rowSelection`是一个对象存在的， 简单分析

- `type`

  可选 `checkbox`  或者 `radio`， 就是样式

- `selectionRowKeys` 

  界面所显示的选中行， 其中`selectedRows`是用来更新点击选中的行， `oldBindList`是服务器返回已经绑定的数据id数组

  显示的时候把他们进行拼接

- `onChange`

  设置每一行的点击事件

- `getCheckboxProps` 方法

  用于自定义设置哪些行是默认不可编辑的， 当然是服务器返回的已经绑定的数据



## 实现每一行的点击方法



### 代码

```JavaScript
handleSelectRows = (selectedRowsKeys, selectedRows) => {
	const oldBindList = this.props; // 获取默认绑定数据

	const newSelectedRowsKey = selectedRowsKeys.filter(
		(item) => oldBindList.indexOf(item) === -1
	);

	const newSelectedRows = selectedRows.filter(
		(item) => oldBindList.indexOf(item.id) === -1
	);

	this.setState({
		// 更新数据
		selectedRowsKeys: newSelectedRowsKey,
		selectedRows: newSelectedRows,
	});
};
```



### 代码解读



- `selectedRowsKeys`

  当前全部选中的行的简单数据， 我这里是个`id`数组， 会随着点击变化

- `selectedRows`

  当前全部选中行的详细数据， 也是随着点击而变化

- `newSelectedRowsKey`

  剔除服务器返回已经绑定的`id`数组， 也就是纯新增的

- `newSelectedRows`

  剔除服务器返回已绑定的完整数据数组， 也是纯新增数据



## 使用自定义`rowSelection`



### 代码



就是在`table`属性里添加这个属性

```JavaScript
rowSelection={{
	...rowSelection,
}}
```



### 代码解读



就是把自定义对象解构使用



## 总结

这样就实现了设置默认选中行且不可取消的界面效果， 并且提交新增数据时候也是剔除原有已绑定的纯新增数据